---
layout: layouts/base.njk
permalink: /blogroll/
title: Blogroll
---

## Here is a list of all the blogs and sites that I frequently (or not so frequently) read.

* [David Revoy](https://www.davidrevoy.com/) / [Pepper & Carrot](https://www.peppercarrot.com/) | [RSS](https://www.davidrevoy.com/feed/rss)
* [The Linux Cast](https://thelinuxcast.org) | [RSS](https://thelinuxcast.org/feed/feed.xml)
* Amin's [TTY1](https://tty1.blog/) | [RSS](https://tty1.blog/feed/) / [Musings](https://benjaminhollon.com/musings/) | [RSS](https://benjaminhollon.com/musings/feed/)
* [RL Dane](https://rldane.space/) | [RSS](https://rldane.space/feeds/all.atom.xml)
* [Ivan](https://libreivan.com/) | [RSS](https://libreivan.com/posts.rss)
* [Adamsdesk](https://www.adamsdesk.com/) | [RSS](https://www.adamsdesk.com/feed/blog.xml)
* JP's [ModdedBear](https://moddedbear.com/) | [RSS](https://moddedbear.com/blog/index.xml)
* [Clayton Errington](https://claytonerrington.com/) | [RSS](https://claytonerrington.com/feed.xml)
* [Daudix](https://daudix.one/) | [RSS](https://daudox.one/atom.xml)
