---
title: 2 I Setup RSS
date: 2023-10-03
tags:
 - Webdev
 - RSS
 - 100DaysToOffload
---

<code>This post was in no way researched :)</code>

RSS is an interesting technology and honestly not everyone seems to know about it. It's an acronym for Really Simple Syndication and allows users to follow a feed of posts from any number of sources. It provides an easier and in my opinion, much better way to get news and blog posts compared with social media.

I setup RSS on all my blogs to provide this easy way to interface with my blog. To do this, I handcraft each <code>.xml</code> file to include the full post, it's the most annoying thing when a feed I follow shows the description of the post then a read here link; hence why I put the full post. I am looking to automatically update the feed through a bash script to save me having to do it each and every time I publish a new post.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
