---
title: 12 - Be Kind Online
date: 2023-10-29
tags:
 - 100DaysToOffload
 - Random Thoughts
---

# * {Enter yelling and insults here} *
Today I had to block someone for just escalating a situation that didn't need to be escalated. Now I accept some responsibility for the arguement, but honestly, I'll let you app be the judge.

It all started when Bitwarden said something about Flatpak being a distribution. I replied saying that Flatpak isn't a distribution, it's a package manager. Then someone replied to me saying that I was wrong and that it meant distribution of Bitwarden (acting asif anyone should know this, which wasn't explicit and doesn't make sense). I responded with I am still correct in the fact that Flatpak is a package manager not a distro so their responses are mute.

Aftet their sarcastic response, I decided to end the discussion with a final remark - something along theines of "Thanks for the sarcastic response and that I was henceforth ending the pointless discussion and blocking them".

Let me know on the Fediverse if I was right in what I did. I engaged a little bit with them but then got fed up and ended things with a block.

[The only arguement I've had on the Fediverse, to date](https://linuxrocks.online/@orbitalmartian/111025588839658378)

The whole point of this post is to emphasise that no matter whether you argree or don't agree with something, or someone, just ignore them or block them, don't start a meaningless arguement or be sarcastic once they have proved their point is correct.

Thanks for reading and make sure to share your thoughts with me on the Fediverse. Also feel free to subscribe to the  [RSS feed of my blog](https://orbitalmartian.codeberg.page/feed/feed.xml). Have a wonderful rest of your morning, afternoon, evening, timezone! 🐼

If you have any suggestions on what you want to see in my blog, hit me up on the Fediverse - [@orbitalmartian@alpha.polymaths.social](https://alpha.polymaths.social/@orbitalmartian)

