---
title: 14 - A Fresh Start For My Laptop
date: 2023-11-04
tags:
 - Windows
 - Windows 11
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111360243444470706
---

# I'm Back!

It's been a week of maybe more since my last blog post, so I'm now trying to get back into the flow of things, [NaNoWriMo](https://nanowrimo.org) is in full swing and I am using a lot of my free time on that project, which you can view a working version of it live on this site - [here](https://orbitalmartian.codeberg.page/blog/nanowrimo/2023/novel/) - enjoy the reading (both my NaNo and this blog post).

I have a total of 4 laptops, and a 5th on the way. Today I'm going to be talking about my main laptop, the one I use for any art I use PC for, for my 3D modelling and for my game development. In the past year, I have neglected to power it on and keep it updated, so after a while of it being slower than normal for a Windows 11 laptop with its specs, I decided, enough was enough, time to update. The updates kept failing, so I decided to reset to factory settings; unfortuneately, it got stuck in a reboot loop whilst on the final installing stages. Luckily a family member is better with Windows than me and so I sent it to them and they got it all reset for me.

I straight away installed my browser of choice [Vivaldi](https://vivaldi.com/) (downloaded via Chrome which they oh so very kindly installed for me, not that I asked). From there I installed WSL and have got a working Ubuntu environment in my Windows system, this is where I'll be doing any [#NaNoWriMo](https://nanowrimo.org/participants/orbitalmartian) work. I then decided to check for updates to ensure it was the most recent update. Took about 12 hours, but we're here, at the most up to date point thus far.

Now that all updates are done, I am now happily starting to reinstall all the apps I want and having transferred all the files that I wanted to take over when I started resetting, I have all I need to start a fresh with this laptop.



***Disclaimer:* Yes, I use a Windows 11 Laptop as my main laptop, however if I wanted to do some proper work here, I'd use either WSL or a Linux VM as I am more comfortable in a Linux environment.**



I'm really running low on blog ideas, if you have any suggestions,  please [hit me up on the Fediverse](https://alpha.polymaths.social/@orbitalmartian).
