---
title: 11 - NaNoWriMo - What Is It? Am I Doing It?
date: 2023-10-28
tags:
 - NaNoWriMo
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111318642981535176
---

# Clickety Click
November is NaNoWriMo and I'm here to answer a couple of questions. What is it? And am I doing it?

## What Is It?
[NaNoWriMo](https://nanowrimo.org/) is a annual month long writing challenge to write a total of 50,000 words on a story (typically new, I believe) throughout the month.

## Am I Doing It?
In short, [yes](https://nanowrimo.org/participants/orbitalmartian). In long, I am tentatively joining the challenge in order to better myself as an author/writer.

I'm really running low on blog ideas, this one was inspired by a comment to me by [Benjamin Hollon](https://alpha.polymaths.social/@amin). If you have any suggestions,  [hit me up on the Fediverse](https://alpha.polymaths.social/@orbitalmartian).

