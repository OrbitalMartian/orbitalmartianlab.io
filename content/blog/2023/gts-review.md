---
title: 21 - I Gave Up On "Normal" Social Media
description: An in depth review of GoToSocial, the lightweight and fast Fediverse software written in Go.
date: 2023-12-06
tags:
- Review
- Fediverse
- GoToSocial
- 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/111535218064450203
---

When people say social media, many people think of Twitter (or X if you're being pedantic), Instagram, or Facebook. However, little do much of the Earth's population know, there is a different side to social networks...

## The Fediverse
The Fediverse is a niche group of softwares who provide a decentralised network of social platforms. In my mind when I think to the Fediverse and explaining it, I see a rather unusual picture.

A group of islands, were each island is a server (or instance), with bridges between them being the federation (defederation is when the bridges get blown up). Each island has it's own language which is the metaphor I'm using for the software.

## GoToSocial
Now let's move on to [GoToSocial](https://gotosocial.org/). GtS is by far my favourite Fediverse software, it's lightweight and much more user friendly compared with other (larger) Fediverse software such as Mastodon and Pleroma. Below are list of my favourite features:

1. No frontend. This may sound counterintuitive but with no front end UI to drag the resources down to the dumps, the software runs as smoothly as I can talk to women (in my dreams). It also means that users are encouraged to use third party clients, personally I'm using Semaphore hosted through Vercel with a fork of the source code hosted on GitHub (I know, eww GitHub, it's just until I can figure out getting it on Codeberg).

2. Easy to use. GtS is very simple with no frontend to confuse things, it is so easy to get started. I don't know about the admin side, but from a user's point of view, it was very simple. At the moment, there is no web sign up page, so server admins have to manually create accounts. [Amin](https://alpha.polymaths.social/@amin) created a script to automatically create them based on an email form thingy (I know, very professional and smart sounding).

I first had the opportunity to test out GtS back when Amin setup his test instance ([gotosocial.verboseguacamole.com](https://gotosocial.verboseguacamole.com)) and I had an [account](https://gotosocial.verboseguacamole.com/@orbitalmartian) setup and I enjoyed testing out the software for a few months. Then once Amin setup the [Alpha](https://alpha.polymaths.social/@orbitalmartian) instance for [polymaths.social](https://polymaths.social). I was among the first batch of users to be created, with  [my now main Fediverse account](https://alpha.polymaths.social/@orbitalmartian). Having now spent over a month now on Polymaths, I can thoroughly say I have enjoyed and will continue to enjoy using GoToSocial and can and will 100% (one hundred percent) recommend GtS.

I am now going to sign off for this post but I will definitely be posting another post this side of Christmas (a Christmas special).
