---
title: Moonlight Bench - Art Timelapse
description: Here is a timelapse for an art piece I made a little while ago, enjoy the process of by far one of my best pieces.
date: 2023-12-03
tags:
- Art
- Timelapse
- Procreate
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/111516795797976227
---

<iframe title="Moonlight Bench - Art Timelapse" width="560" height="315" src="https://peertube.linuxrocks.online/videos/embed/e78b5675-4651-4630-a4ff-05ac7d765311" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<p>{{ page.data.description }}</p>

