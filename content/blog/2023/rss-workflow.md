---
title: 18 - RSS Is The Past, Present And Future
date: 2023-11-20
tags:
 - RSS
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111456057380780743
---

There is something amazing and relaxing about reading blog posts, whether with a purpose or not. Most people would go to each site individually and sift through the site to find any new posts. However, not for me, nor a few people who I know from the Fediverse.

We used RSS feeds instead.


## What is RSS?
RSS or Really Simple Syndication is a feed that allows users or applications to get updates from websites, commonly used by blogs, news feeds and Git repos. It is a simple (as it says in the acronym) system to easily get your favourite posts and articles in one place.

## My Workflow
There are numerous reasons why I use RSS feeds to consume my favourite content, the main one being the simplicity of the centralised location. This allows me to see all new blog posts/articles when they are published, rather than having to go to each website individually and trying to remember which I've read and which haven't.

Another reason I use RSS over individual sites is that it allows me to have a consistent theme and aesthetic across each post. I can only use dark themes with accent colours, my eyes strain when using light themes. Typically each site has its own theme, its own choice as to whether light or dark theme is most accessible; however for me personally I need a dark theme. In my opinion, sites should have a theme selector to allow users to make their own decisions on this subject.

The last reason is that RSS feed readers allow me to keep track of which posts I wish to read and have not previously read marked as unread. This helps me to stay organised with my reading list, of course, there will be some one off articles that I don't want to subscribe to a feed for but still want to read, I need to figure out a way to read them without subscribing to the site's feed.

## Conclusion
In conclusion, RSS is an irreplacable part of my digital life now, and it also encourages me to read more posts in order to learn more (I'll be doing another post on the benefits of reading soon) and offers a relaxing activity to maintain your positive wellbeing, both mental and emotional. If you like reading blog posts, and could prefer having all posts in one place and having full control over how your feeds appear, then maybe using a feed reader and RSS is for you.

Personally, I use a web based feed reader called Miniflux (hosted by Amin), although I have previously used FreshRSS and desktop based ones such as Akregator (from KDE).

I'm really running low on blog ideas, if you have any suggestions,  [hit me up on the Fediverse](https://alpha.polymaths.social/@orbitalmartian). Also leave a comment down below through the Fediverse.

