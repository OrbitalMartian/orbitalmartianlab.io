---
title: 15 - Aliens Are Amongst Us!!!
date: 2023-11-17
tags:
 - Random Thoughts
 - 100DaysToOffload
 - Aliens
 - Astrophysics
mastodonThread: https://linuxrocks.online/@orbitalmartian/111428319375740099
---

# Weooooopppp
I'm going to be trying out a new style of posts here on my blog, science related and some may be opinionated more than scientific. Today I'm going to talk about aliens.

Aliens could be little green men from Mars, but they could also be unimagined creatures from hundreds of millions of light years away and to them, the Earth doesn't exist yet. If you travel ~4.2 billion light years from Earth, our Solar System has not yet formed. Yes, loght years are a unit of distance, not a measure of time, it's related to the speed of light. It can get very confusi, let's take a closer look at light years, from London to Edinburgh it's 5.6476E-11 light years, it's an interesting concept and is difficult to process in your mind.

Anyway, back to aliens, alien species could even be as small as a single cell organism, similar to bacteria, nobody truly knows what aliens will look like or where they reside in the universe, or whether they actually exist (of course, there is the possibility to implement the Schrödinger's Cat principal, where a cat is placed in a box with a vial if poison that could release at any moment, and without opening the box, you don't know whether the cat is alive or dead, so it's theoretically both dead and alive).

Anyway, that's enough brainfarting for one day, hope you enjoyed and I'll catch you around or in the next one 👋 .

I'm really running low on blog ideas, if you have any suggestions,  [hit me up on the Fediverse](https://alpha.polymaths.social/@orbitalmartian). Also leave a comment down below through the Fediverse.

