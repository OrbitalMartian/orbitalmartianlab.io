---
title: 1 - Mad Ramblings of Your Local Martian
description: I wanted to get a good headstart to 100DaysToOffload but all that came to me was random thoughts of little importance.
date: 2023-10-02
tags:
 - 100DaysToOffload
 - Ramblings
---

Today I had a thought, about the universe, and it's sheer scale. The universe is full of planets, stars, rocks and man made objects, however they are surrounded by a vast expanse of empty space, but noone can explicitly state how large that space is. And to think that on a satellite image of Earth, we as humans are not visible, then look at the size of Earth in a picture of our Solar System, that just shows how large that expansive land is.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
