---
title: 19 - Why I Use iPhone Over More Open Alternatives
description: I use iPhone despite advocating for FOSS software, here's why.
date: 2023-11-26
category: Tech
tags:
- Apple Ecosystem
- iPhone
- 100DaysToOffload
---

I got my first smartphone when I was between 14-16, can't remember exactly but it was a hand-me-down iPhone 5s (which was already handed down from my parents to my grandparents before it came to me). Ever since then I have had iPhones. And yes, I know I claim to advocate for Free and Open Source software, but I have one of (if not the most) closed source mobile OS out there.

The reasons for which, are really rather simple: it's what I was given. I was given or bought the phones as gifts and iPhones was what I was given. And since I first started with iPhone, all of the software and my workflow are now engrained into my brain and have become second nature. The second reason is because it just works, I used it and it worked for me (mostly, I'll get onto the negatives in a minute). If I want to send a message, I can send a message, if I want to call someone, I can. Many apps I use (if not web based) are available for iOS as well as my PC OS of choice (whether that be Windows or Linux). This means that I can keep doing what I'm doing on any device I please.

Now onto the negatives. One negative is that it's closed source, which as a lover of everything open source is a problem. Another issue is that most apps that I use are not able to be easily transferred to iOS, typically they require a full scratch up rebuild which most developers don't have the resources to do so, same for getting it onto the App Store, it costs an arm and a leg to get an app onto the store and most devs say "hell nah!". Apart from those negatives, it's an extremely positive experience.

I have had 3 iPhones:
  1. iPhone 5s (2018/19***ish***-2020)
  2. iPhone 7 (2020-2023)
  3. iPhone 14 (2023-present)

Now, of course, it won't be for everyone, but for me it works and one day I may try Android and Linux phones but for now iPhones work for me. Let me know your thoughts in the comments (through the Fediverse).


