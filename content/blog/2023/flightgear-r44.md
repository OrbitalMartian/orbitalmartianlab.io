---
title: 9 - Robinsons R44 Raven in Flightgear
date: 2023-10-25
tags:
 - 100DaysToOffload
 - FlightGear
 - Flight sim
---

# *caw caw*
[Flightgear](https://flightgear.org) is my favourite flight simulator, and today I've enjoyed flying the [Robinsons R44 Raven](https://wiki.flightgear.org/Robinson_R44). Here's my first thoughts about the aircraft.

I've been flying the R44 for about a couple of hours now, total flight time (total time in aircraft in sim) of 03:09:20 and effective flight time (time actually in air) of 02:08:00. I enjoyed a nice flight from Lee-On-Solent (EGHF) to Lydd (EGMD) to North Weald (EGSX), a nice little jolly around England.

FlightGear is an amazing way to enjoy the world from the comfort of your own home.

Sadly I didn't manage to grab any screenshots today, so I can't sh
