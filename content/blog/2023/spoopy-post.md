---
title: 7 - Spoopy Season
date: 2023-10-24
tags:
 - 100DaysToOffload
 - Random Thoughts
---

# BOO
I am now going to spend a little minute waffling on about random things.

This month is a spooky month (for some), however I dislike the whole spooky/Halloween stuff. I have never really seen what the point of it all is, it seems like just a corporate scheme to get lots of money on decorations and sweets (so called candy, as you Americans called it).

Anyway that's my mind on it, I really don't like it nor want to celebrate it, catch you around.

If you have any suggestions on what you want to see in my blog, hit me up on the Fediverse - [@orbitalmartian@alpha.polymaths.social](https://alpha.polymaths.social/@orbitalmartian)
