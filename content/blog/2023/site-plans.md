---
title: 17 - Site Plans - Changes & Ideas
date: 2023-11-19
tags:
 - Website
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111439284399719775
---

I have ideas in my head for this site, and today I'm going to share my ideas in this post in order to be fully open and transparent with my audience (however small). At the moment I am using the base Eleventy blog template repo as a base for this here site, however I feel like I want to either make my own theme or use a different theme for the site. Let me illustrate through the power of description and adjectives what my plans are.

## The Site Itself
The site itself will remain here on Codeberg using Eleventy, however I want the theming to be:
 1. Have a Dracula, Everforest, Catpuccin, Rose Pine theme.
 2. Have each blog link on homepage and archive page in a box with title, description, publish date and reading time within each box.
 3. Potentially a theme selector, similar to Freedom To Write's one.

## The Content
In regards to content, I want to publish at least one blog post each week, sometimes more, if I can think of what to write. I'm also considering making my novel into a series of episodic short novels that all link together, similar to a TV show (like my personal favourite, The Big Bang Theory).

On another note, subscribe to my blog on your feed reader of choice, however the XML feed is broken at the moment, at present the only working option is JSON. I am working to figure out what is wrong.

Anyway, that's me for today, make sure to follow [my Fediverse account](https://alpha.polymaths.social/@orbitalmartian) :)

I'm really running low on blog ideas, if you have any suggestions,  [hit me up on the Fediverse](https://alpha.polymaths.social/@orbitalmartian). Also leave a comment down below through the Fediverse.

