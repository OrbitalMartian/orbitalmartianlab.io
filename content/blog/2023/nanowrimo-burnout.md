---
title: 16 - Running Out Of Steam For NaNoWriMo 2023
date: 2023-11-18
tags:
 - NaNoWriMo
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111433235873119894
---

# Burn baby burn
At the end of October, I set about on a challenge to complete [NaNoWriMo](https://nanowrimo.org), a month long event where the goal is to write 50,000 words towards any writing project you please. A few days in I decided to change my personal goal to 5,000 words. Now, 18 days in, I have failed to write any number of words for the last few days. I think that today, I am going to withdraw from formally completing the challenge. I will still work on my novel, and update my word count on the NaNoWriMo site, as well as my blog post [here on my blog](https://orbitalmartian.codeberg.page/blog/nanowrimo/2023/novel.md); but I am not going to be pushing myself to achieve 5,000 words this month. However, if I do reach that goal, I will be happily pleased.

Anyway, that's it, and I'll be doing more blog posts towards ***#100DaysToOffload***. Thanks for reading and I'll see you in the next one, BYE 👋 .

I'm really running low on blog ideas, if you have any suggestions,  [hit me up on the Fediverse](https://alpha.polymaths.social/@orbitalmartian). Also leave a comment down below through the Fediverse.

