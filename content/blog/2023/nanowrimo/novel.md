---
title: Lunar Voyager
date: 2023-11-01
updated: 2024-03-16
category: Writing
tags:
  - NaNoWriMo
  - NaNoWriMo2023
mastodonThread: https://linuxrocks.online/@orbitalmartian/111365919251007778
---


|  |  |
|--------------|-----------|
| Total Words: | 2364 |
| Goals | 5000 |

<p class="notice">⚠️ !Warning! ⚠️ This novel does not have full scientific accuracy, please do not think it does, or comment about this!</p>

<p class="notice">⚠️ !Anonuncement! ⚠️ NaNoWriMo is now over, but I am still workiing on this novel so keep checking back for updates!</p>

<p class="notice">Here be dragons! This is my notice box!</p>

# Lunar Voyager
## Chapter 1
Colenak wandered through the woods near the spaceport everyday on his way to work. He was constantly amazed by the breathtaking views he got of rockets launching into the cosmos far and wide.

Colenak is a tall blue skinned alien, of a species that noone knows too much about. This being said everyone who met him believed that he himself knew more than he was letting on; never did he mention any family, or his home planet. Noone even knew when he arrived, he just suddenly appeared one day on Earth; noone wanted to question it either, they felt too disrespectful to ask him. Everyone who knew him as a close aquantance, knew he lived with his one and only friend, Dvork, who is another alien creature but from a species that is well know, named Awerts originating from the planet Qwert where greenery makes up majority of the planet, compared with Earth's mass being water.

#### Elsewhere...

Dvork was waiting at their window, looking out towards a woodland path, patiently awaiting a visitor. Colenak was heading home from his day job at the spaceport's museum and visitor centre and Dvork had a breathtaking opportunity for the two space nerds. Suddenly there was a knock on the door. Dvork jumped, and peered round the corner, it was Cole. Quickly moving along the hall and down the stairs, move so smoothly, it was like he was gliding along. Swinging the door open, Dvork jumped into the doorway and yelled,

*"I have amazing news!"*

Colenak didn't respond as he saundered through the door and went straight to his quarters.  Dvork turned so their gaze followed their friend  as he slammed the door behind him and disappeared through a hole in the wall which, after a couple of seconds, and a few taps on a console inside the room, the missing wall section reappeared and set itself flush with the wall.

They went back to their room and tried to link to Colenak's voice comms loop (the pair installed a communication system so they can talk when in separate rooms, a loop is the receiver and a hook is the connector). Dvork tapped a couple of buttons and was immediately taken into a comm channel, he remained quiet for a moment, waiting for Colenak to start the conversation.

*"What do you want, D?"*, Cole said, with a bad vibe surrounding his voice.

*"I had some good news that is for the both of us, and when you got back, you were all irritable and stormed off"*, Dvork spoke calmly as to not aggravate the situation further.

Colenak responded, seemingly calmer now:
*"Alright then, what's your big news"*

Dvork's end crackled as they exited their room, and ran to the wall, tapped a few different buttons on their wrist and the wall section proceeded to slide away and reveal a giant room that is full to the brim with tech products and devices that one can only imagine from the future. D was un-perplexed as they wandered in and approached Cole, who was at at a white desk, with a projected computer setup, and nothing on the desk except from a thick pile of paperwork.

D was suddenly aware that Cole had something on his mind, and suddenely found himself pondering whether to question it or give the good news. They decided to ask, in the hope that their news would cheer him up.

*"What's this?"*, Dvork queried, pointing at the pile of papers.

*"That's our orders from the space center"*, he replied.

*"Orders, what orders? And why are the space center giving YOU orders, you don't work for them? ... WAIT! OUR ORDERS?!"*, Dvork suddenly realised what Cole had said.

*"They want us to go to the Moon and setup a base, they have no astronauts willing to go on a Lunar voyage, all they want is to go to Mars or beyond."*, Colenak didn't seem as excited as Dvork  but that would soon be cleared up.

*"Well, what was the news that you had?"*, Colenak tried to bring the conversation back round to Dvork's original topic.

*"Oh yeah, it doesn't matter now that these orders, have come through. But there was an opportunity advertised around the university about this very mission, I was thinking we should apply, clearly we didn't need to"*, Dvork was mentioning the university that he worked at as an astrophysist.

Colenak and Dvork talked for hours about their orders, and after a while they decided to turn in for the night, ready for a briefing in the morning, at the space center. Dvork had returned to their own quarters and opened their journal, though they struggled to start writing. They finished writing after about an hour of trying, eventually giving up after a few sentences. They both had a restful night sleep and awoke early in the morning and met each other in the communal galley for breakfast. The pair barely looked at each other, let alone spoke for the duration of their breakfast meal. Finishing their meal, the two friends stood almost simoltaneously and moved towards the door, with their prepacked bags, they left; the door locking automatically behind them. They walked with a purpose along the woodland path from the previous day with such a bounce that had never been seen in either of the pair. Dvork had never seen Colenak so cheerful since they met, many years ago.

---

Far away inside a secure facility in the middle of the African Savannah, a group of scientists and engineers are huddled around a engineer sketch of a spacecraft. With confused expressions on their faces, they all looked between each other and the diagram. On a large screen there was a flashing red error message, clearly from the spacecraft, noone could seem to be able to figure it out, appearing out of the back of the pack a low grade engineer asked a question.

*"Has anyone checked for sentrifical alignment of the engines?"*, the junior engineer cracked a smile at his own question.

*"Zwerq, shut up, you're just a junior"* screeched a tentacled alien creature, clearly more senior than the Zwerq character she mentioned.

After 20 odd minutes of back an forth debate as to the relevance and {word i forgot, relevance and importance and trueness of it} of what Zwerq had proposed, they finally actioned something that allowed them to proceed further. It also made the senior management aware of the true potential of this junior engineer. Oddly, he felt appreciated for once in his career, other engineers jeered and mocked him, primarily because of his nerdy hobbies, then because he was new to the field. Feeling closer and closer to being pushed out of the industry, Zwerq felt a shiver diwn his spine as he realised he might have a shot to stay in and further his career.

---

Colenak and Dvork reached the security checkpoint after a few hours walking, the security guards stared at the pair with a confused expression, clearly not expecting these people to be the crew that were to be going in. They reached a gatehouse, which they entered and went through scanners and bag searches like those at airports, and then were confronted by a guard sat at a desk surrounded by bulletproof glass.

*"Should you boys be here?"*, the guard shouted.

Cole grabbed the papers from his satchel and and pushed them up against the glass, surprised, the guard takes a long, hard look at the content.

*"Everything seems to be in order, proceed through these doors and have some ID ready."*

**bzzzzzzzz** A sliding door opened to their right and they walked cautiously through, an ID checkpoint.

*Brilliant*, thought Dvork, *I have never been more anxious in my life*.

The ID check went painlessly, and the pair were escorted through the facility until they reached a door, large, wooden texture, with a round handle right in the centre. An aid to the side, stepped out of a cavity in the wall and opened the door to a mamoth office, larger than anyone could imagine; it was like a large hotel suite in there. Dead centre there was a large table sitting on one of the 20 chairs was the Chief of the OSA (Orbital Space Agency), Billiam Corelin.

*"WELCOME! WELCOME! How wonderful it is to see you. Come, come, take a seat."*, Billiam exclaimed, obviously excited to see the pair. In the amazement as they entered the office, the friends failed to notice three other people sat at the table with Bill (as his friends call him).

*"This is Zwerq, Pilot Captain Officer Johansen Belling and Director Jenny Semaphere of Space Laboratories Inc"*, Bill introduced the three strangers to the pair. He seemed ecstatic to have all these people in one place, clearly they were all important to the plan to be set out during the briefing this morning. Director Semaphere stood and stuck her hand out to shake with each of the newcomers, clearly high up on the government's payroll.

*"Let's get started, shall we?"*, Bill was keen to get the briefing started before any pleasantries could be exchanged. He grabbed a presenting clicker from the desk and pressed a button towards the top left of the remote. A screen slid down from the roof and came to a stop at the bottom of the table.

*"Good morning everyone, this is a briefing for the mission to the Moon. Codenamed Lunar Voyage, a crew of 4 will setup shop on the planet and identify the best spot for a permanent base on the Moon"*, he clicked a button and the next slide of the presentation appeared.

*"The crew; Pilot: PCO Belling, Engineer Specialist: Zwerq, and crew members: Colenak and Dvork. You will be living in close quarters presumably until we can get a larger supply ship up to you, when you get there, we will provide you with as much materials as we can send on the craft with you."*, Billiam continued on.

Two hours on and Billiam was wrapping up, *"And that is my conclusion of the briefing, we'll have a 5 minute break and then reconvene for a Question and Answer session then it'll be a get to know you session for all of you crew members, and much more afterwards, including your suits."*

After a break, where all of the people in the room got drinks and snacks before continuing on to the next section of the day. The crew started introducing themselves to each other and found out a lot about them. The first thing asked of them was *What do you normally do for a living?*, the responses were varied as they all came from different backgrounds and careers. There was Colenak working at the Space Center Museum and Visitors Centre, Dvork who was an Astrophysicist at the local university, Zwerq who was an engineer and Johanson who was a pilot and a trained astronaut. The Q&A session, previously, went as well as Bill had expected, many questions about why them for this mission and what are we actually doing there; and of course Bill knew the answers to each and every question that was asked. The crew moved from the table to a small couch circle and had a relaxing chit chat, before their next experience of the suit fitting.

The four of them seemed more relaxed now they had met everyone and knew they were all in the same boat, as opposed to how they all felt as they went into the morning's briefing, where they were all nervous and had no idea what was happening, all except Zwerq. Billiam and Director Semaphere walked back in to the office as the conversations drew to an end. Ready to move things on in the day, he whistled to get everyone's attention.

*"Time for your suit fitting! Everybody, follow me!"*, he shouted, well not shouted more said with a slight loudness to his voice, obviously still happy with the plan that was now in motion.

The group followed Bill through the numerous winding, confusing corridors, to a set of rooms with biometric protected doors. Opening into a white room filled with people meandering between workstations filled with blue and orange suits, with extra materials on a further table and helmets on another. Bill turned round to face the crew, spreading his arms as if introducing a new product at a conference.

*"This is the suit suite! It's so exciting to have all of you in here at last."*, he had clearly been waiting for this moment for a long time.

A small team of seven suit fitters came over as soon as they saw the group enter, ready to greet them and start the process. Everything seemed to be meticulously planned and everyone knew where they were going to be and were expecting them. Colenak thought to himself about his childhood, back on his home planet and how noone expected him nor wanted him, and how different this situation was to his journey from birth to adulthood. He for once felt involved and wanted and this brought a knot to his throat, and a burning in his heart. In the past, since being on Earth, he hasn't felt emotions or understood why he felt the way he did, however with this personal growth, he is one step closer. Bill walked around (seemingly aimlessly) between each of the team and just watched, he didn't make eye contact with the crew members nor did he speak to anyone.

Several hours passed with the crew sat in the same seats, staying completely still so that the fitters could get on their job. Cole realised they had been there for longer than they'd realised, none of them had noticed when the team completing the fitting had left the room. Things became all silent, eerily so. Noone was too sure what to do, until finally Cole stood up, followed by D. The crew discussed amongst themselves and decided to go for a walk through the facility.




