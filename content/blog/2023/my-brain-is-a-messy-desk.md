---
title: 3 - My Brain Is A Messy Desk
date: 2023-10-04
description: My brain is a complete mess, here's how I easily explain it.
tags:
- 100DaysToOffload
- Random Thoughts
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/111552648737915271
---

<p>Today, I have a little annacdote to explain how my brain works, as this is all I can think about. I like to think of it as a desk, all my thoughts are pieces of paper, placed all over the show without any sense of order, but I know where everything is. Controlled madness, indeed! Whilst at work, my desk gets the same, my day starts out nice and tidy (both my brain and my desk), then eventually things grow and find their places, it's just how I work.</p>
<br>
<p>If you have a topic you'd like me to cover, let me know via <a href="https://alpha.polymaths.social/@orbitalmartian">Fediverse</a> and I'll see what I can do.</p>
<br>
<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
