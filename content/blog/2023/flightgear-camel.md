---
title: 8 - Sopwith Camel in Flightgear
date: 2023-10-25
tags:
 - 100DaysToOffload
 - FlightGear
 - Flight sim
---

# *spit*
[Flightgear](https://flightgear.org) is my favourite flight simulator, and recently I've enjoyed flying the [Sopwith Camel](https://sites.google.com/view/fgukhangar/flightgear-uk-home-page/hangar/davinci-aircraft/warbirds/davinci_wwi_fighters?authuser=0) (by [VooDoo](https://sites.google.com/view/fgukhangar/flightgear-uk-home-page/hangar/davinci-aircraft/warbirds?authuser=0) at [FGUK](https://fguk.me)). Here's my first thoughts about the aircraft.

I've been flying the Camel for about a week now, total flight time (total time in aircraft in sim) of 07:31:24 and effective flight time (time actually in air) of 02:02:10. I enjoy flying around the UK, especially with the Orthophotos that I was given in the [FGUK forum](forum.fguk.me). FlightGear is an amazing way to enjoy the world from the comfort of your own home.

<br>

If you have any suggestions on what you want to see in my blog, hit me up on the Fediverse - [@orbitalmartian@alpha.polymaths.social](https://alpha.polymaths.social/@orbitalmartian)

