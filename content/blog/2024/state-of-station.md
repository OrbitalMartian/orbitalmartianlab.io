---
title: State of the Station
description: Things have been changing, and that's not going to change anytime soon, here I discuss the changes and where we are at.
date: 2024-11-08
tags:
- Webdev
- Website Remake
- Eleventy
---

Hey folks, it's been a little while since my last post, today I am going to explain what the current situation is with my website rebuild. In my last update blost, I said I had started the move, I can now say that majority of the changes have been made, I am working
through a few issues, but majority of what I want is moved over, majority of my blosts have moved, and all the features that I wantd to migrate have been transferred, I'm just working on the last few.

I also want to touch on a __big__ change, the location of my website. As you can see, it is no longer hosted on Codeberg through Codeberg Pages. This is because I wasn't too happy with the levels of downtime both Codeberg and CB Pages had. The Codeberg Fediverse account responded to my concerns saying that Pages wasn't fit for purpose. I have moved the source of my website to a [Gitlab repo](https://gitlab.com/OrbitalMartian/orbitalmartian.gitlab.io/) and the site is then being built using Netlify to this site here.

A nice simple way to do things, I am going to work on some automation, like I said in a [really old blost](/blog/2024/automating-blog/). I want to make my life easier to ensure that I can focus more on my writing as opposed to dedaling with all the nitty gritty of the backside coding.

I hope you enjoyed this update, I may do more dev updates soon, maybe I'll make a Readable CSS Eleventy starter project, who knows. I feel like I want to do some more development side of things as well but not for my own site :). Anyway, have a good one and I'll catch you in the next one.
