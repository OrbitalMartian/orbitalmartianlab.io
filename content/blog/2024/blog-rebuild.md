---
title: 28 - I'm Rebuilding My Blog
date: 2024-01-25
tags:
 - Blog
 - Webdev
 - 100DaysToOffload
---

I have switched my blog so many times, neorly as mony times as I've switched Fedi instances. However, I've found my (for now) home, 11ty. I now need to find all my old blog posts from #100DaysToOffload so I have from number 1 not just from number 3.

I've had a quick look around and I can't find them, hopefully it's cached in my RSS feed, else they'll be lost forever. :( I will continue writing new posts to fulfill my brain farts and my attempt for 100DaysToOffload.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
