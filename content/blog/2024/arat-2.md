---
title: A Race Against Time - Episode 2
date: 2024-07-18
draft: false
---

<aside><p>This blost is crossposted from <a href="https://arat.codeberg.page">the official ARAT site</a>.</p></aside>

08-AD ran back through the market street that we saw them walk through with their  friends earlier, heading towards their house. In their mind, a plan was beginning to form. They needed to find out what grandpops was talking about, Dashad had an inkling of an idea but couldn't be a hundred percent sure. Inside their house, a small round creature started jumping excitedly from one side to the other, clearly sensing Dashad's pending arrival. This creatures name is Snoogle and he was a Snugglepuff, a creature found only in the rainforests of East Defint Bay; hundreds of thousands of them were tamed by the first settlers of the region and sold them, bred them, and left a small population alone in the rainforest.

Snoogle hopped towards the door and continued his little jumping routine until finally...

Dashad swung the door open with a **SLAM!!!**

"Come on Snoogle, we have got to get going, I just need to grab somethings from my room.", said Dashad as they walked past the round ball of colourful fur. Moving through the doorway to their room, they grabbed a rucksack from just inside the right wall. Slinging it onto the bed, in the centre of the room, they looked around and grabbed a load of supplies and some food that was on a desk against the far wall.

Five minutes after arriving home, Dashad and Snoogle walked and hopped out the door and Dashad closed it behind them, locking it and putting the key into their bag. The duo walked with purpose back through the market street once again heading towards the town wall, that has stood for the last couple of centuries protecting the town from the outside world.