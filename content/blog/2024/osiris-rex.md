---
title: 39 - The Journey of the Space Dinosaur
date: 2024-03-08
tags:
- Space
- 100DaysToOffload
---

> https://benjaminhollon.com/musings/all-hail-osiris-rex/

I was scrolling through some old posts by [Amin](https://alpha.polymaths.social/@amin) and came across this post and thought I'd write a bit about this momentous occasion.

Back in 2016 NASA launched a an astroid return mission, with the OSIRIS-REx probe aboard a Saturn V rocket from Launch Complex 41 at Cape Canaveral. The mission was planned to last for 7 years. In late 2023, the probe returned to Earth with the sample, noone knew exactly what it would bring back or whether it even worked.

On the day of the landing, there was a mass of activity around Utah Test and Training Range from numerous helicopters to a high altitude reconissonse airplane. The recovery teams were on tenderhooks whilst awaiting the reentry of the sample capsule, unsure as to whether it would successfully touch down or if it did whether there would be any samples inside.

At 6am UTC on 24<sup>th</sup> September 2023 the spacecraft ejected the sample return capsule at an altitude of 63,000 miles from Earth, entering the atmosphere at a speed of 27,650 mph. At 15.52 UTC the capsule touched down on the Utah Test and Training Range, 1 minute earlier than expected. The ground team went about their tasks to ensure that everything was safe, ready for moving the capsule. A Bell 206 helicopter longlined the capsule to the clean room.

<figure>
<img src="/img/IMG_2963.jpeg" alt="OSIRIS-REx Capsule on the ground in Utah">
<figcaption>OSIRIS-REx Capsule on the ground in Utah</figcaption>
</figure>

<figure>
<img src="/img/IMG_2964.jpeg" alt="OSIRIS-REx Ground Team pacing round the SRC">
<figcaption>OSIRIS-REx Ground Team pacing round the SRC</figcaption>
</figure>

Once they moved the SRC (Sample Return Capsule) into the clean room, they found they had extra sample on the outside of the capsule. The research they do around this will help shape the future of space exploration, and our knowledge of the universe on a whole.

I still, to this day, remember tuning into the NASA live stream on YouTube and watching this historical event unfold in real time. My inital thoughts were that this was such a huge operation that involved so many people who must've felt so incredibly proud.

Oh and as I come towards the end of writing this blog post I realise I haven't talken much about the asteroid that the samples were collected from. Astroid 101955 Bennu which is a carbonaceous near-Earth astroid situated ~103,328,671 kilometers away from Earth. The spacecraft travelled that whole way collected a number of samples from a few landing sites and then started it's journey back to Earth.

<figure>
<img src="/img/IMG_2947.gif" alt="Astroid Bennu">
<figcaption>Astroid Bennu</figcaption>
</figure>

This is a topic of great interest to me and I hope that if you find this interesting too you reach out to me via email. I hope you enjoyed and I'll catch you in the next one.

<aside>
<p>All images in this post were sourced from <a href="https://en.m.wikipedia.org/wiki/OSIRIS-REx">Wikipedia</a>.</p>
</aside>

---

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>


