---
title: 35 - Automating My Blog
description: Making my life easier is as simple as shebang, script, run.
date: 2024-02-18
category: Tech
tags:
- Webdev
- 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111955062737371495
---

I have enjoyed figuring out what I need to do to make things on my site here. Now I've figured the manual way, I am working on automating things to make my life easier. This all came around from me playing around with bash while trying to make a really simple blog system using templates and a bash script.

This being said, what are my planned changes around here. Most, if not all are going to be my side without anything being too visible to the folks that read my blog.

* First, I'm going to be creating a ```_templates``` directory with template files such as template posts, template pages, and any other templates that I may need.
* Second, I'm going to create a ```_scripts``` directory to house all my automation scripts.
* Thirdly, is the scripts themselves. I am planning on having a few of them; the ones I've planned in my head already is a new post script to offer me prompts for title, description, date (if left blank use that day's date), etc and also calculate read nime by counting the words and doing a rough calculation using a predefined time to read one word. Another I have planned is for new pages, so if I want to create a new, non blog page it'll give me prompts and automatically copy the templates with correct names, etc.

Now these plans are large and will take some time to actually do but that's one problem that just happens to come with progress like this, and once it's all done It'll make my life a lot easier.

I also plan on adding Readable.css into my site, I just need to work out some kinks, then I may be able to implement a theme switcher, which is all very exciting.

