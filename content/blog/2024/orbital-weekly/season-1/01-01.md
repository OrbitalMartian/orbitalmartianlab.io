---
title: Orbital Weekly - Episode 1
description: "Orbital Weekly is my podcast in words, I guess Wordcast you could call it. I have no idea what to write right now so here we go, I will discuss some topics that interest me each week in a nice blost like this"
date: 2024-08-09
draft: false
tags:
- Orbital Weekly
- Linux
- Gaming
- Flashing Lights
- Cosmic
- Desktop Environment
- Google
- Web Extensions
- Adblock
- 100DaysToOffload
---

Welcome to the first episode of **Orbital Weekly**.

---

<div class="note">
	<h2>What Is Orbital Weekly?</h2>
	<p>Orbital Weekly is my "podcast in words", I guess Wordcast you could call it. I have no idea what to write right now so here we go, I'll discuss some topics that interest me each week in a nice blost like this</p>
	<p>If you have any topics you think it would be worthy to read up on or to write about, <a href="mailto: orbitalmartian@tuta.io">send me an email</a> with the link to the article or website and I'll take a look</p>
</div>

---

## Contents
<ol>
<li><a href="#cosmic-desktop-alpha">Cosmic Desktop Alpha</a></li>
<li><a href="#flashing-lights-ai-update-part-1">Flashing Lights AI Update - Part 1</a></li>
<li><a href="#google-kill-adblocks">Google Kill Adblocks</a></li>
</ol>

### Cosmic Desktop Alpha
Let's kick things off with an exciting piece of tech news. [System76](https://system76.com/) has finally released the new Cosmic Desktop, written in Rust, which, from the screenshots that I've seen, looks absolutely amazing and I will definitely give it a try later on. The System 76 team have been working tirelessly for the last 2 years to build a whole new shell, desktop environment, Iced toolkit, app set and a Wayland compositor from scratch. I will include a few screenshots from the System 76 website doen below.

I am honestly quite excited to see where the DE goes as it's the first Wayland only desktop which is an interesting way to go (somewhat controversial for some people, but I see a good DE in Cosmic). We'll see in the coming months and years as the project progresses.

![Cosmic Desktop Alpha screenshot - desktop](https://images.prismic.io/system76/ZrOL3UaF0TcGIwTO_cosmic-sectionlifestyle-desktop.png)
![Cosmic Desktop Alpha screenshot - tiling](https://images.prismic.io/system76/ZrONd0aF0TcGIwUN_cosmic-sectionlifestyle-adaptable.png)

Cosmic also has auto tiling builtin, which looks like a pretty full fledged feature at this stage, but I have not tried it myself yet.

### Flashing Lights AI Update Part 1
I have in the last month or two been playing Flashing Lights and on the 4th July, they released a new update which brings AI partners which is a big thing. Giving players usable backup, something which has been missing in the game so far. The AI is not the best and they aren't the smartest but they offer support to players on calls. I have given the feature a try on the Police and EMS teams. The EMS team partners actively work on the patients and even get things out the car/ambulance. The Police partners are armed with just a pistol, not the taser, shotgun and rifle that players have access to.

Police partners struggle to get through doors and I haven't had much luck with them being helpful but being a kinaesthetic learner, I didn't much look at the documentation and just jumped straight into playing the game.

![Flashing Lights - EMS AI Partners](https://clan.akamai.steamstatic.com/images//30559558/a671d87fe4905373255ed3445f3476b07c91652c.jpg)

### Google Kill Adblocks
This topic has been in the news of the tech space for a little while now, but as the time draws ever closer, I feel the need to talk about Google removing Manifest V2 which is used by all the adblock extensions, meaning that all adblocks (such as AdGuard and Adblock Plus) will stop working. This will be a big problem for those of us who don't want the websites we visit to be flooded with adverts which have little to no relevance to us at all.

I understand, however, that browsers like Vivaldi, with their own builtin adblocker, will still work as they aren't built on Manifest V2. I know that this is something thay Google has got to do to make way for Manifest V3, but they could have given the developers a bit more access to create all the features. Adblocks like uBlock Origin have already created an alternative using V3 but it has had to be a stripped down version with less features as the new Manifest does not allow certain things (frim what I've found.

Needless to say, Google is going the right way to lose its "cult" following.

## Conclusion
And that brings to the close the first episode of Orbital Weekly, my new blog project, below you will find links to all the topics I discussed today, and a link to submit your ideas for future episodes.

I hope you enjoyed and I'll catch you in the next one.

## Links
To submit an idea for a future episode, there are two ways to go about this.

1- [FOSS and Private Form](https://app.formbricks.com/s/clzn9vafj0000sttu567ghgq9)

2- <a href="mailto: orbitalmartian@tuta.io">Email Me</a>

<ol>
<li><a href="https://system76.com/cosmic" target="_blank">System 76 Cosmic webpage</a></li>
<li><a href="https://steamdb.info/patchnotes/14932772/" target="_blank">Flashing Lights AI Update - Part 1 - SteamDB</a></li>
<li><a href="https://news.itsfoss.com/google-chrome-disable-extensions/" target="_blank">Google Chrome Will Soon Disable Extensions like uBlock Origin</a></li>
</ol>

---

This is blost 71/100 for [#100DaysToOffload](https://100daystooffload.com)

