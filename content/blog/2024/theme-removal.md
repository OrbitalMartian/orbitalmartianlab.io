---
title: 29 - I'm Removing My Site Themes
date: 2024-01-26
description: I have been having an existential crisis on which SSG to use.
tags:
 - Blog
 - Webdev
 - Theming
 - 100DaysToOffload
---

Quick note on my last post...

> I've had a quick look around and I can't find them, hopefully it's cached in my RSS feed, else they'll be lost forever. :(

I have since found these posts and will get them transferred over ASAP.

---

I have encountered some issues with theming and am considering removing it completely, however, I don't remember how I added it, but also I like the theme I have right now, maybe I'll go back to barf? Maybe... I don't know anymore, which site do you prefer, let me know on [this poll](https://alpha.polymaths.social/@orbitalmartian/statuses/01HN3T7QRE1RXADRX2S42DVGDC). I am thinking Barf as it's so simple, maybe I'll addsome cool features over there and setup auto building.

Enjoy and I apologise if I do switch again as the RSS link will change again XD - might be able to cobble something together to combat this.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
