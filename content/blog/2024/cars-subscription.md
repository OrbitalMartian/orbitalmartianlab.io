---
title: 24 - All Cars Have Subscriptions
date: 2024-01-14
description: A random thought about cars and subscription services.
tags:
 - Vehicles
 - Ramblings
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/111754787341153122
---

Today, during my driving lesson I thought about subscription services and how gears are similar.

As you go up in gears, you unlock higher revs. Same way as subscription tiers work.

That's it for this post, I'm trying to get back into posting once a day :)

<a href="https://100daystooffload.com" class="--center">#100DaysToOffload</a>
