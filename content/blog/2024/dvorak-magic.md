---
title: 23 - I Switched To Dvorak
date: 2024-01-13
description: QWERTY isn't the only option out there.
tags:
 - Dvorak
 - Keyboards
 - Challenge
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111750902764392893
---

There are many keyboard layouts out there, but majority of users use QWERTY which was modelled after old fashioned typewriters.

A few days ago, I set myself a little challenge and successfully completed it. The challenge was use Dvorak for 24 hours (on my phone).And it was successful, I've enjoyed the challenge of learnung the new layout and I can tell you now it was difficult, I'm still working on my speed and reducing the amount of mistakes I make, but I enjoyed myself and am continuing to use it.

I have been working on this post for a few days and as of now it has been around 4 days in order for me to focus and actually write this garbage, hope you might have enjoyed, catch you later folks.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>