---
title: My Birthday - 2024
date: 2024-06-08
draft: false
tags: 
 - Ramblings
 - 100DaysToOffload
description: It's my birthday and I'll cry if I want to.
---

Today is my birthday, and yes it is now the evening and I have only just written this.

I am not going to reveal my age here but some people across the web do know. I am an adult so there isn't too much that I would want. Let me share what I got, which I will be trying out tomorrow.

My main present was a gaming chair, not a known brand but it's a really comfortable chair. Back and neck cushion and actually fit with my desk better (size wise). Another thing that I got was a wired controller (the product name says XBox but it works on PC too), as well as some other things such as Lord of the Rings book 1 (because I got 2 and 3 from a charity shop and wanted to start with book 1). I also got a bunch of chocolate, some money, a pen and a cushion (for comfort).

I will be playing with my new toys (this is a turn of phrase). Have a good one folks!

---

This is post 61/100 for [#100DaysToOffload](https://100daystooffload.com).
