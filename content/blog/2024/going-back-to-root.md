---
title: Going Back To Root
date: 2024-06-23
draft: true
tags: 
 - 100DaysToOffload
 - Terminal Use
 - DecapCMS
description: After a week or so using DecapCMS, I'm going back to root.
---

DecapCMS was great, but I started using it before getting it all setup causing numerous issues here and there. I have had to make the decision to go back to using the terminal and Neovim to write my blosts, whilst I try to get another CMS setup and working correctly before I start using it.
