---
title: I'm Not An Expert
date: 2024-08-10
draft: false
tags:
- Ramblings
- 100DaysToOffload
---

With my new blost series, Orbital Weekly, now under way, I have felt the need to write this. I am by no means an expert in any one field. I am a polymath. A person who has knowledge in lots of different places. For example, I have no knowledge on Google's extensions or anything like that so my words on that are subject to my understanding of my research, so may not be 100% correct or accurate.

I am trying to make sure that everything I write is correct and if I find out that I am wrong, then I will add a correction to the post. If you notice an error, please let me know and I will make the edit.

Thanks for reading and I hope to write some more in the next few weeks.

---

This is post 72/100 for [#100DaysToOffload](https://100daystooffload.com).
