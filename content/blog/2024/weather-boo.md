---
title: 25 - The Weather Is Bad
date: 2024-01-14
description: Rambling about the weather in the UK.
tags:
 - Weather
 - Ramblings
 - 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/
---

I have just checked the weather for my local area and it's currently 3°C, but there are times tomorrow that we're forecast -1°C, that's fudging cold.

I would like some more snow as it's a pretty sight, but it also causes chaos on the roads and elsewhere, everyone goes nuts when there's a tiny bit of snow. There are some places in the UK that are forecast snow over the coming few days but not, as far as I can see, for my town/area.

Anyway that's me for today, hope you enjoyed! Catch you all later folks.

<a href="https://100daystooffload.com" class="--center">#100DaysToOffload</a>
