---
title: Bookshelf First Look
description: "I have found a great app on iOS to help readers manage their Bookshelf."
date: 2024-08-10T18:12:14Z
tags:
- iOS
- Reading
- Review
- 100DaysToOffload
draft: false
---

A few days ago, I took a look back through ny notifications on my Sharkey account, and found a message from Matt ([The Linux Cast](https://thelinuxcast.org)) saying that he used an app on iOS called [Bookshelf](https://apps.apple.com/us/app/bookshelf-your-virtual-library/id1464032274). I have for quite a few months now, used Bookwyrm to record my reading, but that also adds the social side of it (which although great, is a bit slow. Bookshelf is a nice, fast app which just works, it does have a social feature but it isn't lile Bookwyrm with posts, it has follow profiles and you just see the books and progress.

I have been using Bookshelf for a few days now and have found it much more helpful to record my reading progress, and it's much easier to use than Bookwyrm. I love the scan ISBN barcode scanner to search books which is the easiest way to make sure I have the correct version of the book that I am reading, have read or want to read.

After a few weeks, I was reading more and more thanks to the app, allowing me to track where I am with my book and keep an eye on how many pages I have read. It's simple things but they really do make a difference (even if it was just personally.

Below you will find screenshots of a couple of the pages in Bookshelf.

![My Reading Shelf](/img/bookshelf-1.jpg)

![Book page for Anthony Horowitz's The House Of Silk](/img/bookshelf-2.jpg)

---

This is blost 73/100 for [#100DaysToOffload](https://100daystooffload.com)

---

After having this blost sit in my drafts since 12/7/2024, I have lost the flow of it, half way through. Apologies for any changes in tone, etc.
