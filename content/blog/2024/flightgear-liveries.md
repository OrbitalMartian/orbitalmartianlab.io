---
title: 38 - My New Found Love Of Liveries
description: I recently started making a load of liveries for FlightGear, here's why and how.
date: 2024-03-02
tags:
- FlightGear
- 100DaysToOffload
---

Recently, I have been enjoying making liveries for various aircraft in FlightGear. I can list a few of the aircraft that I have worked with: EC135, Bell 206, BAe Hawk T2, Beachcraft King Air B200 and many more. I have found it relaxing working on these. There is a certain chill factor to this kind of art.

## Why?
Sometimes I want to fly a certain airline, company, air force which aren't included with the aircraft. I therefore make my own, recently I created a Wiltshire Air Ambulance and British Airways helicopters liveries for the Bell 206.

## What's The Process?
Firstly, I grab a current livery and then paint on a new layer. Then, once I feel it's ready, I copy a current liveries' xml file and edit it as required, mostly it's just changing the name and the paint path. After the code is done, I load it up in the sim and if there are any issues, I make the changes and try again.

I use Procreate to make all my liveries and then export them as a .png and transfer from my iPad to my gaming laptop for testing and use. Other prople use alternatives like GIMP or Photoshop but I prefer using Procreate as it's an easier option for me.

Anyway, that is me for today, I'll catch you in the next one, feel free to drop me an email and checkout all [my available liveries](/fglivs).

---

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>


