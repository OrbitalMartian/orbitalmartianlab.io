---
title: 40 - Join The Light Side
date: 2024-03-09T22:44:00Z
tags:
 - Light Mode
 - Technology
 - iOS
 - Phone
 - 100DaysToOffload
---

I have, for a long while now, always used a dark theme of some sort. It was very relaxing for my astigmatism and reduced the strain on my eyes. Recently, I decided to give light themes a go, my phone is now set to light mode and QWERTY as keyboard as opposed to Dvorak.

Since making the switch, I have felt a bit less stress on my eyes, especially when I'm tired which, for some reason, recently has been all the time. Just the light mode alone has helped me to read things easier.

Further to the theme switch, I have switched my phone's keyboard from Dvorak on GBoard to QWERTY on the stock iOS keyboard. Not 100% sure why I did this but I have and it has helped me write faster but I do keep going to try typing with Dvorak positioning, which makes me want to go back to it and keep on there.

In terms of whether I'm going to stay with light theme, I have no idea. It might end up being like my SSG and Fedi instances where I change every (what seems like) 5 minutes so we'll see.

That's it for today, so I'll see you in the next one.

---

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
