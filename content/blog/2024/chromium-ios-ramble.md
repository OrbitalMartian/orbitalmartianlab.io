---
title: 34 - Chromium Needs To Be On iOS
date: 2024-02-09
description: I ramble about me wanting Chromium on iOS.
tags:
 - Web Browsers
 - Chromium
 - Ramblings
 - 100DaysToOffload
---

I know Google has Chrome on iOS, but why not Chromium having Chromium - I love using Chromium based browsers (yes I know that all browsers on iOS are Webkit based) but I don't want to have to use Chrome on iOS if I can use Chromium on my laptops.

There are instructions on the Chromium site for building to iOS but that needs a Mac and I'm not getting one, nor do I have one.

Just my daily mad ramblings.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
