---
title: My First Impressions of Wolf 359
date: 2024-05-28
tags:
- Podcasts
- Wolf 359
- 100DaysToOffload
draft: false
---

<aside>
<p>Earlier today, I wrote this whole blost and set it all up ready to publish, clicked 'Commit' and poof all my work had gone, so if things here seem rushed or wrong or angry, this is why.</p>
<p>And with this out the way, it’s time to put this into the past and move on to the actual post</p>
</aside>

In recent weeks, I have gotten back into listening to podcasts with [Status: Untraced](/blog/status-untraced---review-thus-far/). After posting on the Fedi, I received a recommendation from [Sotolf](https://alpha.polymaths.social/@sotolf) for a Science Fiction show called [Wolf 359](https://wolf359.fm/); so with this I thought I’d give it a listen, and here I am today, writing this, 15 episodes in. 

Wolf 359 is a science fiction podcast show, featuring 4 main characters living on a space station orbiting the star of the same name. Here is what the show's description on my podcatcher says:

> Life's not easy for Doug Eiffel, the communications officer for the U.S.S. Hephaestus Research Station, currently on Day 448 of its orbit around red dwarf star Wolf 359. He's stuck on a scientific survey mission of indeterminate length, 7.8 light years from Earth. His only company on board the station are stern mission chief Minkowski, insane science officer Hilbert, and Hephaestus Station's sentient, often malfunctioning operating system Hera. He doesn't have much to do for his job other than monitoring static and intercepting the occasional decades-old radio broadcast from Earth, so he spends most of his time creating extensive audio logs about the ordinary, day-to-day happenings within the station. But the Hephaestus is an odd place, and life in extremely isolated, zero gravity conditions has a way of doing funny things to people's minds. Even the simplest of tasks can turn into a gargantuan struggle, and the most ordinary-seeming things have a way of turning into anything but that. Wolf 359 is a radio drama in the tradition of Golden Age of Radio shows. Take one part space-faring adventure, add one part character drama, and mix in one part absurdist sitcom, and you get Wolf 359.


I started listening to the show yesterday whilst I was flying in the sim, and thoroughly enjoyed the listening experience. The characters took me a couple of episodes to figure out, but the story itself has been really easy to follow. I was told that the first 10 or so episodes aren't as good and not as enjoyable, but I honestly enjoyed every minute of it.

Now I’m on episode 20, and still enjoying it just as much as episode 1. Listening to the show has also got me motivated to continue writing [A Race Against Time](/a-race-against-time/), my mini story series.

That’s it for this one, enjoy!

---

This is post 58/100 for [#100DaysToOffload](https://100daystooffload.org).