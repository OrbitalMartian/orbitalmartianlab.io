---
title: 43 - XMPP Is Better Than Matrix?
description: I tried XMPP today, here is my thoughts.
date: 2024-04-02
tags:
- XMPP
- 100DaysToOffload
- Software
- Socials
draft: false
---

I have been trying to get into using open source software for my socials, hence Fediverse and Matrix being my primary social networking. I've heard of XMPP before and only decided to give it a go this morning. I found a server/instance and created an account. I've reached out to one of my FediFriends who I know their XMPP address and have made contact.

## What is XMPP?
XMPP is a simple messaging service, comparable to SMS, Matrix or Discord. Being so small it (just like GoToSocial) doesn't have a built in client (like Mastodon, Matrix and Discord), so you have to find a client for yourself, I've found a couple of good ones: Converse.js (web) and Milon (iOS/iPadOS). Being a simple minded martian, I instantly fell in love with the software.

## Why?
XMPP can be used as a simple means to keep in touch with your friends, or, for those who could implement into a work environment, colleagues. So simple that even I got an account up and running with contact with another account with ease.

Things like this give me hope for the wider Fediverse and decentralised social networking. Anyway, that's all for this one, if you enjoyed let me know in <a href="mailto:orbitalmartian@proton.me?subject=RE: XMPP Is Better Than Matrix?">email</a>.




