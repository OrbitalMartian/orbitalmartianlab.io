---
title: Refactor Started
date: 2024-10-26 12:06:00 +1
draft: false
tags:
- Website Remake
- Eleventy
- Webdev
mastodonThread: https://vmst.io/@orbitalmartian/113373435113400083
---

After my [blost yesterday](/blog/2024/website-rebuild-plans/), I have started the refactoring, it's a bit harder than I first though, but Iworked away last night and this morning on getting a base setup. I got a basic, basic working last night but I am working on making it more mine by building more to it.

My new plan is to look at my current site and rewriting all of the code that's there, removing all of the old code and commented out things. Also along the way, I'll document the code so that I can easily rebuild without having any issues like not knowing where to start.

I'll share some screenshots and more information as I do more work. Maybe later today. Have a good one, catch you all later.
