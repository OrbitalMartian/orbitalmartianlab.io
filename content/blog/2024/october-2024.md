---
title: October 2024 Month Review
description: October was a gaming month, here's the details.
draft: false
date: 2024-11-03
tags: 
- Month Review
- 100DaysToOffload
---

A busy month for gaming, a tiny amount of reading and a ton of gaming. October has been quite busy.

## Reading 
I finished reading Lancaster: The Forging of a Very British Legend and continued reading The House of Silk, I'm now 254 pages (65%).

## Gaming 
The last two weeks alone had me flying 24+ hours total in FlightGear. I also played Flashing Lights quite a bit.

## Writing
This month, I wrote 4 blosts and have been working on [my new site](/alpha/).

# Podcasts
I listend to quite a few episodes of [The WAN Show](https://www.youtube.com/playlist?list=PL8mG-RkN2uTw7PhlnAr4pZZz2QubIbujH) and an episode of [The Linux Cast](https://thelinuxcast.org/).

## Conclusion
A nice busy month, time for the next one.
