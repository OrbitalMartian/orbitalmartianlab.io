---
title: 37 - Fancy New Name For My Blog?
description: I have always wanted a fancy name for my blog, now's the time to consider the options.
date: 2024-02-27
tags:
- Website
- 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/112005128665114307
---

Today I had a very small idea, change the name of my blog. I have always wanted a fancy name for my blog, now is the time to consider the options.

Originally my blog was called OrbitalBlog, however presently it's called OrbitalMartian. I have a few ideas in my mind but I need to fully explore each one before making my decision. I have a few things to consider before creating any names...

### 1. What's my niche?

Well, I am hoping to aim my blog more towards tech, art and gaming, but I also feel that there will be some general random stuff thrown in there. This could cause issues, but that's where I can add duplicate tags and use them as categories (tech, art, gaming and other).

### 2. Who's my audience?

Well, to be completely honest, I don't actually know. However, if I were to predict, I would go with young adults who are enthusiasts and hobbiests for the topics that I cover.

### 3. Schedule?
Well, although I'd love to have a rigid schedule for my blog posts, I don't feel that I would be able to maintain such a schedule.


### Name Ideas
Recently I tried making my own static site generator which I came up with a name for so that has given me one idea for my site's name: Qongrex (an alien name).

Another is something todo with flight simming. I love flying and I all of my recent videos have been FlightGear videos. I have no ideas on this one.

My final idea is to keep the name the same, OrbitalMartian fits me and I like it. This also reduces the amount of work that I need to do, which also means that my RSS link won't change (many of my readers will love this).

Anyway, that's me for this one, I've been writing this since `29-11-2023` and have only just got round to finishing it at the end of February 2024.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>


