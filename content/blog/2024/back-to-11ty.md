---
title: 44 - Back To 11ty?
description: I'm moving back to my 11ty site.
date: 2024-04-13
tags:
- 100DaysToOffload
- Ramblings
<!-- mastodonThread: /* Mastodon link, if included */ -->
---

After spending a while on my **beautiful** [Hugo site](https://orbitalmartian.gitlab.io/), I am making the decision to move back (yes I know, I'm moving blog again).

## Why The Change?
Well, I recently moved away from 11ty to Hugo which was a great change, but looking back at my **[wonderfully designed](https://readable-css.freedomtowrite.org/)** site, I felt the urge to come back, and after spending much if the late evening, early night trying to fix a couple of issues with this site, I fell back in love with the simple ways of my 11ty workflow (which gives me an idea for another blog post).

## What Does This Mean?
Apart from the obvious, main site links moved back to here, and moving all my blogs posts from Hugo over to here, there's also the RSS folks (sorry). All the die hard tech trolls out there (including myself) who use RSS to follow my blogs will need to make sure that this [link](https://orbitalmartian.codeberg.page/feed.xml) is what their feed reader is pointing to for my posts, else you'll get nothing (might be for the best :) ).
Things won't change too much, I have created a nice Rose Pine theme for the site (and that's what it's staying as for the time being), but some little tweaks may be made.

---

After this long winded way of saying Welcome Back To My Life 11ty, I hope you enjoyed my complete waffle and I'll see you on the Fedi. [(Leave me a comment here)](https://alpha.polymaths.social/@orbitalmartian/statuses/01HVCR7N363WJ9Z7Y78ZW29FP4)

<aside><p>
Oh and also forgot to mention, I have moved my main Fedi account to my <a href="https://alpha.polymaths.social/@orbitalmartian">GtS Account</a> from my LinuxRocks Mastodon account, this means all LR links will most likely not work for a bit whilst I allow all my followers to move to Polymaths.
</p></aside>

