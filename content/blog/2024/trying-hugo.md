---
title: 41 - I'm Giving Hugo A Go
description: I explain why I'm giving Hugo a go as an option for my website.
date: 2024-03-29
tags:
- Webdev
- Hugo
- 100DaysToOffload
---

I've been thinking in the last few days about my website. I have been using Eleventy for the last long while, and although I've changed the look of it through Readable.css, I feel that I want to make a change. Hugo seems a nice, simple option and I, today, found [Zaney's blog post](https://zaney.org/posts/install-hugo-and-deploy-to-gitlab/) on how to install and create a site, which I am (at the time of writing this) going through and following. The system is reasonably quick and I feel that with enough time spent on the initial setup, it could be a nice easy website for my blog. His guide leads through using Gitlab, I'm going to try using Gitlab, but if not I'll try converting the script to run on Codeberg (similar to what Clayton did for Eleventy, which I use to build this here blog).

I'll post more updates on my Hugo journey on one of my Fedi socials, probably [Polymaths.Social](https://alpha.polymaths.social/@orbitalmartian).


