---
title: I Am Still Alive
date: 2024-07-16
tags:
- Ramblings
- 100DaysToOffload
draft: false
---

Hello, it's been a while since my last blost. So here I am saying I am still alive, I've just been working on my [CMS project](https://codeberg.org/OrbitalMartian/BashCMS/). Having made a load of features in one way, I have recently started redesigning it and have already got the new post and edit existing post features implemented.

I have been enjoying reading the last few weeks and months, and in the more recent days, I have been yearning to get back into doing art. This may come into fruition in the near future, who knows.

Hope you enjoyed, and have a good one!

---

Blost 68/100 for [#100DaysToOffload](https://100daystooffload.com)
