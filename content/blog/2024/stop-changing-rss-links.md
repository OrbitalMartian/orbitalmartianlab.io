---
title: My Site Is Having A Facelift
date: 2024-07-24
draft: true
tags: 
 - 100DaysToOffload
 - Web Development
description: I have decided to play around with the look of my site, so have given and am continuing to give my site a bit of a facelift.
---

For over a year now, my website had been using [Readable.css by Freedom To Write](https://readable-css.freedomtowrite.org/) as it's base for styling. I have, in the last couple of days, switched the base css of my website to [Simple.CSS by Kev Quirk](https://simplecss.org). The design is nice and modern but also really small by design and I love the aesthetic.

The main things that I have changed and am planning to change are the navbar (which is going to join the aesthetic of simple, but modern), the post archive list will be in <code>article</code> tags which will aid this aesthetic. Other things include a latest blost link in the footer and updated [contact page](/about/stalker) and new pages such as [more](/more-items/) which is visible in the navbar for mobile users, I don't think it's working as it should've but it's a cool design.

Enjoy and have a good [insert timezone here]!

---

This is blost 69 (noice)/100 for [#100DaysToOffload](https://100daystooffload.com).
