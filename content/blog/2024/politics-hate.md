---
title: Why I Hate Politics On The Fediverse
date: 2024-10-27T12:33:00
draft: false
tags:
- Politics
- Ramblings
mastodonThread: https://vmst.io/@orbitalmartian/113379435225809408$0
---

Last night, I made a [Fediverse post](https://alpha.polymaths.social/@orbitalmartian/statuses/01JB536868GC1S0DF7RNV1EF1R) about politics.

I feel the need to explain why I posted that and what my views on it is. If you don't want to read this blost about politics, please feel free to leave now.


I try to stay away from politics, I have little to no views on politics, as long as shit doesn't hit the fan, I'm happy. I have never voted nor do I have any wishes to. I follow people on the Fediverse for Tech, Art, Gaming things and Polymaths people too, not for politics, which sucks when people you follow post politics as well as the content you want. I don't want things to be dictated by me but I, as a UK person, don't care about US politics as it doesn't affect me (please can the US people not give me the crap of US is the world, runs the world, etc, etc).

I honestly hate politics as it causes so many issues, hence why I made the post.

I think that's enough said, I'm sorry for writing about politics, but I felt I had to. Have a great rest of your day. Bye for now.
