---
title: Fediverse Account No. 4001
description: I just started yet another Fediverse account.
date: 2024-07-25
tags:
- 100DaysToOffload
- Rambling
- Fediverse
draft: true
---

Over my years on the Fediverse, I have (very possibly) held the record for the most alt accounts, which has become a running joke amongst my FediFriends. I have got quite a few accounts, and today I went and started another one. This time I decided to go [vmst.io](https://vmst.io) which I have heard of as one of my FediFriends has an account there.

## Why Do I Have So Many?
I  always like to jump around here and there, and with 