---
title: September 2024 Month Review
# description: August was an interesting one, here's what I was up to.
draft: false
date: 2024-10-21
tags:
- Month Review
- 100DaysToOffload
mastodonThread: https://vmst.io/@orbitalmartian/113345272426831039
---

September was busier than other months, but less so than others. Here's a rundown of the month.

## Reading
I read a 126 pages in September, all from the book "Lancaster: The Forging of a Very British Legend", really enjoyed this reading. Not as much as I could've done.

## Gaming
I have played many hours of Flashing Lights and FlightGear, even recording and releasing a few Flashing Lights videos on [YouTube](https://youtube.com/@orbitalmartian).

## Writing
I wrote 5 blosts in September, and sadly, I didn't get to any other writing.

## Conclusion
A quieter month than normal, now onto the next one.
