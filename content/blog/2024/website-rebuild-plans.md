---
title: I'm Remaking My Site From Scratch
date: 2024-10-25
draft: false
tags:
- Webdev
- Website Remake
- Eleventy
mastodonThread: https://vmst.io/@orbitalmartian/113369719155781483
---

As of today, I am planning to remake my website (yes this one here) from scratch.

## Static Site Generator
I have tried many static site generators (SSGs) in my time with a website, including, Jekyll, Eleventy, Hugo, Barf, Zola and hand writing websites. I have chosen to (for now) stick with Eleventy as this is what I have got setup and working at the minute. Eleventy offers many features which I personally like, such as Custom Pages, Templates and a large number of addons.

## Why?
The biggest reason is because my current website's codebase is really full of spaghetti code and based on the eleventy-base-blog-starter repo and I personally feel that I could streamline a lot of the code which I don't use or need. This could lead to some issues but I have a week off work so I am going to spend some time working on this in the next week.

## Timeline
I am hoping to get things to a decent, stable level by next Tuesday, which will give me a few days to transfer content and work on any little issues that I find running it on a "alpha" state site. I am hoping to get it all done and live by the end of next week, a big task but I will 100% be working on this full time (as well as gaming) throughout the week.

## Testing
I will be locally testing all of my work before releasing onto my "alpha" site, this will allow me to catch any big bugs and issues and allow me to keep everything functional. I plan to use Readable CSS which I know is as stable as it gets so the theming will be top notch, thanks to [Amin](https://alpha.polymaths.social/@amin).

---

Anyway, that's all I'm going to write about it today, I will post about my progress on [my Fediverse account](https://alpha.polymaths.social/@orbitalmartian) and here, so please do follow along if this interests you. I am hoping that this will all go well. This site will not be going down until I am 1000% sure that the new one is working to it's best.

That's it from me and I'll catch you in the next one, see ya.
