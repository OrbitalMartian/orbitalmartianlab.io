---
title: 26 - Matrix has a nice community, but is it too small?
date: 2024-01-22
description: Matrix is great but is it too small.
tags:
 - Matrix
 - Ramblings
 - 100DaysToOffload
---

I've been using Matrix for a while and I've enjoyed it. Many a time, I've enjoyed exciting and engaging conversations. Whether it be in a  pixel art software community or in a room of friends. This is similar to how I use the Fediverse, always trying to get involved in the action.

The one issue is that there aren't that many communities on Matrix. lots are on Discord but that's very closed source and proprietary but it would be nice if more people were on Matrix. They would, of course, need to work en some more moderation tools that would encourage people to join and move to Matrix from other places like Discord.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>
