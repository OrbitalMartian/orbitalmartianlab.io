---
title: 45 - iOS Features That Are Good
description: iOS has some bad stuff in it but there is some good amongst the bad.
date: 2024-04-14
tags:
- iOS
- 100DaysToOffload
---

<aside>
This post is not about why I am using iOS but on the good features of iOS.
</aside>

So as I mentioned in a [previous post](/blog/apple-use/) that I use an iPhone, which obviously runs iOS. There are many features which are absolutely terrible, but there are some great features.

Here is a list of all the features that I am mentioning so you can skip through to any that you want.

#### Table of Contents
1. [Privacy warnings](#privacy-warnings)
2. [Shortcuts App](#shortcuts-app)

#### Privacy Warnings
So today, I accidently opened my camera and then closed it and I was greated by an orange dot on the right of the camera island, opening the Control Centre I was greeted by a warning telling me that the camera had recently used my camera and microphone.

<figure>
<img src="/img/privacy-warning.jpg" alt="iOS Control Centre with Privacy Flags">
</figure>

#### Shortcuts App
I love customisation and unfortunately, there isn't any support for themes or icons. (Why are you saying this, this post is meant to be the good things) Well yes, but custom icons can be set using the Shortcuts app.

And here is a screenshot of my current custom icons (made using Canva).

<figure>
<img src="/img/custom-icons.PNG" alt="Custom Icons on iOS">
</figure>

And though I wish I could think of more features, I can't right now, so I'm going to leave it there, and start working on my next story (of which I have an idea). Have a good one folks and leave me a comment on the Fedi.

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>

