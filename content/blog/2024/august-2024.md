---
title: August 2024 Month Review
description: August was an interesting one, here's what I was up to.
draft: false
date: 2024-09-18
tags: 
- Month Review
- 100DaysToOffload
---

I have had a busy one in August, not as busy as other periods, but still busy enough. From reading to blogging and gaming, a month full of things to talk about.

## Reading 
First up I want to talk about reading. In August, I continied reading The House of Silk by Anthony Horowitz, but put on pause after I received Lancaster: The Forging of a Very British Legend by John Nichol which I am 302 pages into (as of 18th September), which is 65% through. Really enjoyable book and a blost about why I'm reading two books is [available here](/blog/reading-double/).

## Gaming 
I definately played a lot this month, I got back into [Minecraft](https://www.minecraft.net/), joining the [Fedicraft server](https://fedicraft.org) and started a brand new single player world with [BSL Shaders](https://www.curseforge.com/minecraft/shaders/bsl-shaders). I played quite a bit of [Flashing Lights](https://store.steampowered.com/app/605740/Flashing_Lights__Police_Firefighting_Emergency_Services_EMS_Simulator/), with this new AI Update Part 1 which does add a lot to the game. And, of course, as always, a load of [FlightGear](https://flightgear.org). 

## Writing
Over the course of August, I wrote 7 blosts for my blog. For those who don't know, blost = blog + posts.

## Conclusion
I appologise for the lateness of this month review blost, things got crazy. Enjoy and I'll see you in the next one.

---

This is blost 81/100 for [#100DaysToOffload](https://100daystooffload.com)

