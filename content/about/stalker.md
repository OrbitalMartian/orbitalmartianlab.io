---
layout: layouts/base.njk
eleventyNavigation:
  key: Stalker
  order: 3
permalink: /stalker/
---

# Stalker
Here is a list of my social links for all my stalkers :) Follow whichever you want.

## Main Accounts
* 🌎 **[GoToSocial](https://alpha.polymaths.social/@orbitalmartian)**
* 🌎 [Peertube](https://peertube.linuxrocks.online/c/orbitalmartian8/videos/)
* 🔒 [YouTube](https://youtube.com/@orbitalmartian)
* 🌎 [Bookrastinating (Bookwyrm)](https://bookrastinating.com/user/OrbitalMartian)
* 🌎 Matrix User - <code>orbitalmartian:envs.net</code>
* 🌎 Email - <code>orbitalmartian@tuta.io</code>

## Secondary Accounts
* 🌎 [VMST Mastodon](https://vmst.io/@orbitalmartian)
* 🌎 [Fosstodon Alt](https://fosstodon.org/@orbitalmartian)
* 🌎 [Akomma Alt](https://pleroma.envs.net/orbitalmartian)
* 🌎 [Shonk Alt](https://shonk.phite.ro/@OrbitalMartian)

## Inactive Accounts
* 🌎 [Social Linux Pizza Alt](https://social.linux.pizza/@orbitalmartian)
* 🌎 [Vivaldi Social Alt](https://social.vivaldi.net/@charl8)
* 🌎 [Treehouse Systems Alt](https://social.treehouse.systems/@orbitalmartian)
* 🌎 [Plasmatrap Alt](https://plasmatrap.com/@OrbitalMartian)
* 🌎 [Dot Art Writeas](https://dotart.blog/orbitalmartian/)

## Dead Accounts
* 🌎 ~~[LinuxRocks Mastodon](https://linuxrocks.online/@orbitalmartian)~~ - Instance Drama has caused me to leave my beloved LinuxRocks, currently looking for another instance for my main Masto account.
