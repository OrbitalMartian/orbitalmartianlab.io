---
title: Past, Present and Future
layout: layouts/base.njk
permalink: /now/
---

## Past, <b><u><i>Present</i></u></b> and Future

Here is what I'm up to right now.

**Last updated: 8th November 2024 @ 23:22**

## Me
- Currently I am living in the UK.
- I'm working within a school in admin.
---
## What I'm Reading
- Currently I'm reading The House Of Silk by Anthony Horowitz.
- I'm enjoying a lot of blogs, mostly small web people - links available in my [blogroll](/blogroll.html).
---
## What I'm Watching
- I'm addicted to The Big Bang Theory (I've got the full show on DVDs).
---
## What I'm Working On
- Right now, I'm working on my blog.
- I've recently fallen in love with making liveries for FlightGear. [Take a look at my finished releases here on my site.](/fglivs/).
<!-- --- -->
<!-- ## What I'm Listening To -->
<!--- I've been getting back into listening to podcasts, recently been enjoying 'Status: Untraced', a true crime podcast about the disappearance of a hiker in India.
- I've been engrossed in a Sci-Fi fiction podcast called 'Wolf 359' which I have been enjoying for about a week now, one of the only things keeping me going.-->
<!-- - I'm not currently listening to any music, nor any podcasts. -->
<!-- --- -->
## What I'm Writing
<!-- - I'm currently writing blog posts towards my first attempt at 100DaysToOffload.-->
- I'm writing a mini story series titled `A Race Against Time` for [Writing Month](https://writingmonth.org/). [READ HERE](https://arat.codeberg.page/)!

---

## What I'm Using
### Desktop:
#### Main:
- Operating System - Windows 11
- Browser - Vivaldi
- Art software - Krita / LibreSprite / Blender
- WSL Distro: Debian 12 Bookworm
	- WSL Apps: 'tut', 'Neovim', 'lynx', 'todoman', 'todotxt-cli'

#### Gaming:
- Operating System - Windows 11
- Browser - Vivaldi
- Favourite game - FlightGear
- Headtracking software - Opentrack
- Flight sim companion - LittleNavMap
- WSL Distro: Debian 12 Bookworm
	- WSL Apps: 'tut', 'Neovim', 'lynx', 'todoman', 'todotxt-cli'

#### Space Station
- Operating System - Fedora 41
- Window Manager - Sway
- Browser - Vivaldi
- Terminal Emulator - `alacritty`
- Favourite Apps - `neovim`, `tut`, `thunar`

### Mobile:
#### Phone:
- Model - iPhone 14
- Operating System - iOS 18.1
- Browser -  Vivaldi
- Fediverse client - Orbitalfore
- Matrix client - Element for iOS
- Special App - iSH

#### iPad
- Model - iPad 10<sup>th</sup> Generation
- Operating System - iPadOS 18.0.1
- Browser -  Vivaldi
- Art software - Procreate / Sketchbook
- Fediverse client - Orbitalfore
- Matrix client - Element
- Special App - iSH



