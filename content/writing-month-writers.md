---
title: Writing Month Writers
permalink: /writing-month-writers/
---

## Writing Month Writers

Below are a list of writers taking part in [Writing Month](https://writingmonth.org/).

| Name | Fediverse | Website |
| :--- | :---      | :---    |
| [OrbitalMartian](https://writingmonth.org/~orbitalmartian) | [Polymaths Social](https://alpha.polymaths.social/@orbitalmartian) | [Orbital Station](https://orbitalmartian.codeberg.page) |
| [Lou Plummer](https://writingmonth.org/~amerpie/) | [Social Lol](https://social.lol/@amerpie) | [Living Out Loud](https://louplummer.lol/) & [Linkage](http://linkage.lol/) & [AppAddict](https://apps.louplummer.lol/) |
