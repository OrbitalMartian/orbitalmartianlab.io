export default {
	title: "Orbital Station",
	url: "https://orbitalmartian.codeberg.page/",
	language: "en",
	description: "I am writing about my experiences as a naval navel-gazer.",
	author: {
		name: "OrbitalMartian",
		email: "orbitalmartian@tuta.io",
		url: "https://orbitalmartian.codeberg.page/about-me/"
	},
	version: "2.0.0-alpha"
}
